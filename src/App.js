import React, {Fragment, useState} from 'react';
import Listado from './components/Listado';
import Formulario from './components/Formulario';

function App() {

    const [ citas, setCitas ] = useState([]);

    const crearCita = cita => {
        setCitas([
            ...citas,
            cita
        ])
    }

    const eliminarCita = id => {
        const lista = citas.filter( cita => cita.id !== id );
        setCitas(lista);
    }

    return (
        <Fragment>
            <h1>ADMINISTRADOR DE PACIENTES</h1>
            <div className="container">
                <div className="row">
                    <div className="one-half column">
                        <Formulario crearCita={crearCita}/>
                    </div>
                    <div className="one-half column">
                        <h2>Administra tus citas</h2>
                        {
                          citas.map(cita => (
                            <Listado
                                key={cita.id}
                                citas={cita}
                                eliminarCita={eliminarCita}
                            />
                          ))
                        }
                    </div>
                </div>
            </div>
        </Fragment>
    );
  }

export default App;
