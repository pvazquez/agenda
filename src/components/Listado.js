import React from 'react'

const Listado = props => {

    return(
        <div className="cita">
            <p>Mascota:<span>{props.citas.mascota}</span></p>
            <p>Dueño:<span>{props.citas.propietario}</span></p>
            <p>Fecha:<span>{props.citas.fecha}</span></p>
            <p>Hora:<span>{props.citas.hora}</span></p>
            <p>Sintomas:<span>{props.citas.sintomas}</span></p>

            <button
                className="button eliminar u-full-width"
                onClick={ () => props.eliminarCita(props.citas.id) }
            >
                Eliminar &times
            </button>
        </div>
    )
}

export default Listado;
