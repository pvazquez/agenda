import React, { Fragment, useState } from 'react';
import uuid from 'uuid/v4';

const Formulario = props => {

    const form_data = [
        {id: 1, name: "mascota", type: "text", class: "u-full-width", placeholder: "Nombre mascota", label: "Nombre mascota"},
        {id: 2,name: "propietario", type: "text", class: "u-full-width", placeholder: "Nombre del dueño", label: "Nombre dueño mascota"},
        {id: 3, name: "fecha", type: "date", class: "u-full-width", label: "Fecha"},
        {id: 4, name: "hora", type: "time", class: "u-full-width", label: "Hora"}
    ];

    const [input, updateInput] = useState({
        mascota: '',
        propietario: '',
        fecha: '',
        hora: '',
        sintomas: ''
    })

    const [ error, setError ] = useState(false);

    //Me guardo las entradas del usuario en el state
    const handleChange = event => {
        updateInput({
            //hago spread del estado actual para no perder los campos anteriores cada vez que ejecuta updateInput()
            ...input,
            [event.target.name]: event.target.value
        })
    }

    //Extraigo los valores
    const {mascota, propietario, fecha, hora, sintomas} = input;

    const handleSubmit = (event) => {
        event.preventDefault();

        //Validacion
        if(mascota === '' || propietario === '' || fecha === '' || hora === ''
        || sintomas === '') {
            setError(true);
        }

        //Eliminar -si hay- mensaje de error
        setError(false);

        //Asigno IDs
        input.id = uuid();

        //Envio a app principal
        props.crearCita(input);

        //Limpiar Formulario
        updateInput({
            mascota: '',
            propietario: '',
            fecha: '',
            hora: '',
            sintomas: ''
        });
    }
    
    return(
        <Fragment>
            <h1>Desde Formulario</h1>
            {error
              ? <p className="alerta-error">Todos los campos son obligatorios</p>
              : null
            }
            <form onSubmit={handleSubmit}>
                {
                    form_data.map(input => (
                        <div key={input.id}>
                            <label>{input.label}</label>
                            <input
                                type={input.type}
                                name={input.name}
                                className={input.class}
                                placeholder={input.placeholder}
                                value={input[props.name]}

                                onChange={handleChange}
                            />
                        </div>
                    ))
                }
                <textarea className="u-full-width" name="sintomas"></textarea>
                <button className="u-full-width button-primary" type="submit">Agendar Cita</button>
            </form>
        </Fragment>
    );
}

export default Formulario;
